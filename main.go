package main

import (
	"fmt"
)

type User struct {
	name string
}

func main() {
	// (Security) Hard-coded credentials are security-sensitive
	user := "root"
	password := "supersecret" // Sensitive
	url := "login=" + user + "&passwd=" + password
	fmt.Println(url)

	// (Security) Using hardcoded IP addresses is security-sensitive
	var (
		ip   = "192.168.12.42"
		port = 3333
	)
	fmt.Println(ip)
	fmt.Println(port)

	// (Bug) All branches in a conditional structure should not have exactly the same implementation
	b := 0
	i := 3
	if b == 0 { // Noncompliant
		doOneMoreThing()
	} else {
		doOneMoreThing()
	}

	switch i { // Noncompliant
	case 1:
		doSomething()
	case 2:
		doSomething()
	case 3:
		doSomething()
	default:
		doSomething()
	}

	// (Bug) "=+" should not be used instead of "+="
	var target, num = -5, 3
	target = -num // Noncompliant; target = -3. Is that really what's meant?
	target = +num // Noncompliant; target = 3
	fmt.Println(target)

	// (Bug) Related "if/else if" statements should not have the same condition
	SwitchWithMultipleConditions(4)

	// (Bug) Identical expressions should not be used on both sides of a binary operator
	v1 := (true && false) && (true && false)
	fmt.Println(v1)

	// (Bug) All code should be reachable
	add(2, 3)

	// (Bug) Useless "if(true) {...}" and "if(false){...}" blocks should be removed
	if true {
		doSomething()
	}
	if false {
		doSomething()
	}

	// Two branches in a conditional structure should not have exactly the same implementation
	switch i {
	case 1:
		doFirstThing()
		doSomething()
	case 2:
		doSomethingElse()
	case 3: // Noncompliant; duplicates case 1's implementation
		doFirstThing()
		doSomething()
	default:
		doTheRest()
	}

	a := 14
	if a >= 0 && a < 10 {
		doFirstThing()
		doSomething()
	} else if a >= 10 && a < 20 {
		doSomethingElse()
	} else if a >= 20 && a < 50 {
		doFirstThing()
		doSomething() // Noncompliant; duplicates first condition
	} else {
		doTheRest()
	}

	// (Code Smell) Boolean checks should not be inverted
	if !(a == 2) {
	} // Noncompliant

	// (Code Smell) Control flow statements "if", "for" and "switch" should not be nested too deeply
	if true { // Compliant - depth = 1
		/* ... */
		if true { // Compliant - depth = 2
			/* ... */
			for i := 1; i <= 10; i++ { // Compliant - depth = 3, not exceeding the limit
				/* ... */
				if true { // Noncompliant - depth = 4
					if true { // Depth = 5, exceeding the limit, but issues are only reported on depth = 4
						/* ... */
					}
					return
				}
			}
		}
	}

	// (Code Smell) "switch" statements should have "default" clauses
	switch i { // Noncompliant - default case is missing
	case 0, 1, 2, 3:
		doSomething()
	case 4, 5, 6, 7:
		doSomethingElse()
	}

	// (Code Smell) "if ... else if" constructs should end with "else" clauses
	if i == 0 {
		doSomething()
	} else if i == 1 {
		doSomethingElse()
	}

	// (Code Smell) Expressions should not be too complex
	if ((true && true) || (true && false)) && false {
	}
}

func doFirstThing() {
	fmt.Println("Do First Thing")
}

func doSomething() {
	fmt.Println("Do Something")
}

func doSomethingElse() {
	fmt.Println("Do Something Else")
}

func doOneMoreThing() {
	fmt.Println("doOneMoreThing")
}

func doTheRest() {
	fmt.Println("Do The Rest")
}

func SwitchWithMultipleConditions(param int) {
	switch param {
	case 1, 2, 3:
		fmt.Println(">1")
	case 3, 4, 5: // Noncompliant; 3 is duplicated
		fmt.Println("<1")
	}
}

func add(x, y int) int {
	return x + y // Noncompliant
	z := x + y   // dead code
	fmt.Println(z)
	return y
}

// (Bug) Variables should not be self-assigned
func (user *User) rename(name string) {
	name = name // Noncompliant
}

// (Code Smell) Functions should not have identical implementations
func fun1() (x, y int) {
	a, b := 1, 2
	b, a = a, b
	return a, b
}

func fun2() (x, y int) { // Noncompliant; fun1 and fun2 have identical implementations
	a, b := 1, 2
	b, a = a, b
	return a, b
}

// (Code Smell) Redundant pairs of parentheses should be removed
func foo(a bool, y int) int {
	x := (y/2 + 1) //Compliant even if the parenthesis are ignored by the compiler

	if a && (x+y > 0) { // Noncompliant
		//...
	}

	return (x + 1) // Noncompliant
}

// (Code Smell) Nested blocks of code should not be left empty
func compute(a int, b int) {
	sum := a + b
	if sum > 0 {
	} // Noncompliant; empty on purpose or missing piece of code?
	fmt.Println("Result:", sum)
}

// (Code Smell) "switch" statements should not be nested
func foo2(x, y int) {
	switch x {
	case 0:
		switch y { // Noncompliant; nested switch
		// ...
		}
	case 1:
		// ...
	default:
		// ...
	}
}

// (Code Smell) "switch case" clauses should not have too many lines
func foo3(tag int) {
	switch tag {
	case 0:
		doSomething()
		doSomething()
		doSomething()
		doSomething()
		doSomething()
		doSomething()
	case 1:
		doSomethingElse()
	}
}
